/**
 * server socket handlers and emitters here. The game API definition.
 */

var UUID = require('node-uuid');
var engine = require('../public/core/engine');
var Gamestate = require('../public/core/gamestate');

var Tower = require('../public/core/towers/tower');
var creepProto = require('../public/core/creeps/creep');
var CreepFast = require('../public/core/creeps/creep_fast');
var CreepSlow = require('../public/core/creeps/creep_slow');
var CreepEnum = require('../public/core/creeps/factory');

var MapGen = require('../bin/map_gen');
var player1 = 0;

// holds all gamestates. key = room uuid
var games = {};


module.exports = function(io){
    // Socket.io will call this function when a client connects,
    // So we can send that client a unique ID we use so we can
    // maintain the list of players.

    // client       server
    // gamepage -->
    //          <-- onconnected(socketIdUuid)
    // join     -->                             //room queue up
    //          <-- gamestart                   //game found.
    // ready    -->
    //          <-- start                       //starts waves and schedules
    //
    //          <-- ttwave
    //          <-- wave_data
    //          <-- sync


    // disconct -->
    // tbuy     -->
    // tsell    -->
    // cbuy     -->

    return function (serverSocket){
        /**
         * Game API
         */

        // Generate a new UUID, looks something like
        // 5b2ca132-64bd-4513-99da-90e838ca47d1
        // and store this on their socket/connection
        serverSocket.userid = UUID();
        console.log('\t socket.io:: player ' + serverSocket.userid + ' connected');
        // tell the player they connected, giving them their id
        serverSocket.emit('onconnected', { id: serverSocket.userid });


        // client emits 'join' event on page load
        serverSocket.on('join', function (callback) {
            //find room


            if (!player1) {
                // host the room
                console.log(serverSocket.userid + ': is waiting for connect');
                serverSocket.room = serverSocket.userid;
                serverSocket.join(serverSocket.room);
                player1 = serverSocket.userid;
                var role = "HOST";
            }
            else {
                // join available room
                console.log(serverSocket.userid + ': is joining the waiting player.');
                serverSocket.join(player1);
                var room = serverSocket.room = player1;
                var role = "NON-HOST";
                //map is scaled by the creep
                var map = {
                    width: 5,
                    height: 10,
                    path: MapGen.generate(10,15)
                };
                games[room] = new Gamestate([player1,serverSocket.userid],[{creepTypes:['CreepFast','CreepSlow','Creep']},{creepTypes:['CreepFast','CreepSlow','Creep']}],map);
                games[room].tick =0;
                io.sockets.in(serverSocket.room).emit('gamestart', games[serverSocket.room].toJson());

                var gamestate = games[serverSocket.room];
                run_wave(gamestate);
               // io.sockets.in(serverSocket.room).emit('gamestart', games[serverSocket.room].toJson());

                //client.query("INSERT INTO games(id, player) values($1, $2)", ['samplegame', player1]);
                player1 = 0;

            }

            callback({role: role, roomId: serverSocket.room});



        });

        // When this client disconnects
        serverSocket.on('disconnect', function () {
            // Useful to know when someone disconnects
            console.log('\t socket.io:: client disconnected ' + serverSocket.userid);
        }); // client.on disconnect


        /**
         *  Game API
         */



//        var target_tps = 20,
//            target_delay = 1000/target_tps,
//            time =0;
//        function run_wave(gamestate) {
//            if(typeof gamestate.start == 'undefined'){
//                gamestate.start =  new Date().getTime();
//            }
//            gamestate = engine.tick(gamestate);
//            time += target_delay;
//            // elapsed = Math.floor(time/target_tps)/10
//            var diff = (new Date().getTime() - gamestate.start) - time;
//
//            setTimeout(function () {
//                run_wave(gamestate);
//            },  target_delay - diff);
//
//        }

        serverSocket.on('tower_buy', function (action) {

            var json_tower = action.towerjson;
            var tower = new Tower(json_tower.x,json_tower.y);

            var player = serverSocket.userid;
            var gamestate = games[serverSocket.room];
            if(engine.buy_tower(tower,player,gamestate)){
                //TODO smaller update object, ie towerbuy data only??

                action.tick = games[serverSocket.room].tick -50;//TODO this is not robust...
                console.log(action);
                io.sockets.in(serverSocket.room).emit('sync', action);
            }
            console.log('player['+player+'] now has ' +gamestate.lands[player].towers.length + ' towers.');
        });
        serverSocket.on('tower_sell', function (data) {
            var towerAction = JSON.parse(data);
            console.log("tower_sell:"+data);
        });
        serverSocket.on('creep_buy', function (data) {
            var creep = Creep.fromJson(data);
            var player = serverSocket.userid;
            var gamestate = games[serverSocket.room];
            engine.buy_creep(creep,player,gamestate);
        });


    };
};
