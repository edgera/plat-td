/**
 * Created by edgera on 11/24/2014.
 */


MapGen =
{
    startingCell: {
        1: function n(cols, rows) {
            var x = Math.floor((Math.random() * cols));
            var y = 0;
            startx =x+.5;
            starty= 0;
            return {x: x, y: y, startx:startx , starty:starty};        },
        2: function s(cols, rows) {
            var x = Math.floor((Math.random() * cols));
            var y = rows-1 ;
            startx =x+.5;
            starty= rows;
            return {x: x, y: y, startx:startx , starty:starty};        },
        3: function w(cols, rows) {
            var x = 0;
            var y = Math.floor((Math.random() * rows));
            startx =0;
            starty= y+.5;
            return {x: x, y: y, startx:startx , starty:starty};
        },
        0: function e(cols, rows) {
            var x = cols -1;
            var y = Math.floor((Math.random() * rows));
            startx =cols;
            starty= y+.5;
            return {x: x, y: y, startx:startx , starty:starty};
        }

    },

    getCandidates: function (cur, cols, rows) {
        var candidates = [];
        for (var i = 0; i < 4; i++) {

            candidate = this.next[i](cur.x, cur.y);

            if (candidate.x < 0 || candidate.x >= cols) {
                continue;
            }
            if (candidate.y < 0 || candidate.y >= rows) {
                continue;
            }
            if (this.candidates[candidate.x][candidate.y] > 0) {
                continue;
            }
            if (this.path[candidate.x][candidate.y] > 0) {
                continue;
            }

            candidates.push(candidate);
        }
        return candidates;
    },

    next: {
        1: function n(x, y) {
            return {x: x, y: y - 1};
        },
        2: function s(x, y) {
            return {x: x, y: y + 1};
        },
        3: function w(x, y) {
            return {x: x - 1, y: y};
        },
        0: function e(x, y) {
            return {x: x + 1, y: y};
        }
    },

    endingCell: {
        1: function n(x, y ,cols, rows) {
            return {x: x +.5, y: 0};
        },
        2: function s(x, y,cols, rows) {
            return {x:x +.5, y: rows- 1};
        },
        3: function w(x, y,cols, rows) {
            return {x: 0, y: y +.5};
        },
        0: function e(x, y,cols, rows) {
            return {x: cols-1, y: y +.5};
        }
    },
    getEndingPoint: function(cur, cols,rows){
        if(cur.y==0) {
            return {x: cur.x + .5, y: 0};
        }
        if(cur.y==rows-1) {
            return {x:cur.x +.5, y: rows};
        }
        if(cur.x==0) {
            return {x: 0, y: cur.y +.5};
        }
        if(cur.x==cols-1) {
            return {x: cols-1, y: cur.y +.5};
        }
    },


    printGrid: function (grid) {
        for (var j = 0; j < grid[0].length; j++) {
            for (var i = 0; i < grid.length; i++) {
                str = String("00" + grid[i][j]).slice(-2); // returns 00123
                process.stdout.write(str + ' ');
            }
            process.stdout.write('\n');
        }
    },

    path: [],
    candidates: [],


//    [
//        0, .5,
//        .5, .5,
//        1.5, .5,
//        1.5, 1.5,
//        2.5, 1.5,
//        3.5, 1.5,
//        4.5, 1.5,
//        4.5, 2.5,
//        5.5, 2.5,
//        6.5, 2.5,
//        7.5, 2.5,
//        8.5, 2.5,
//        9.5, 2.5,
//        10, 2.5
//    ]
    generate: function (cols, rows) {

        var validMapCreated = false;
        var pathForApp = [];
        while (!validMapCreated) {
            pathForApp = [];
            for (var i = 0; i < cols; i++) {
                this.path[i] = [];
                this.candidates[i] = [];
                for (var j = 0; j < rows; j++) {
                    this.candidates[i][j] = 0;
                    this.path[i][j] = 0;
                }
            }

            //pick starting boarder
            var dir = Math.floor((Math.random() * 4));
            start = this.startingCell[dir](cols, rows);
            var step = 1;
            console.log(start);
            this.path[start.x][start.y] = step;
            pathForApp.push(start.startx);
            pathForApp.push(start.starty);
            pathForApp.push(start.x +.5);
            pathForApp.push(start.y +.5);
            step = step + 1;
            cur = start;
            mapCandidateFinished = false;
            while (!mapCandidateFinished) {
                var candidates = this.getCandidates(cur, cols, rows);

                if (candidates.length == 0) {
                    //check for valid map otherwise fail map and try again.
                    mapCandidateFinished = true;
                } else {
                    var dir = Math.floor((Math.random() * candidates.length));
                    cur = candidates[dir];
                    this.path[cur.x][cur.y] = step;

                    pathForApp.push(cur.x +.5);
                    pathForApp.push(cur.y +.5);
                  // console.log(pathForApp);
                    step = step + 1;

                    for (var i = 0; i < candidates.length; i++) {

                        candidate = candidates[i];
                        this.candidates[candidate.x][candidate.y] = 5; // number of times before we can try to use again.
                    }
                    //decrement usage thingy.
                    for (var i = 0; i < cols; i++) {
                        for (var j = 0; j < rows; j++) {
                            this.candidates[i][j] = this.candidates[i][j] - 1 < 0 ? 0 : this.candidates[i][j] - 1;
                        }
                    }

                }

            }
            validMapCreated = true;
            if (!(( cur.x == 0 || cur.x == cols - 1) && (cur.y == 0 || cur.y == rows - 1))) {
                validMapCreated = false;
            }
            if (step < rows*cols/3) {
                validMapCreated = false;
            }
        }
        endPoint= this.getEndingPoint(cur,cols, rows);
        pathForApp.push(endPoint.x);
        pathForApp.push(endPoint.y);


       // this.printGrid(this.path);
       // console.log(pathForApp);
        return pathForApp;
    }



};


module.exports = MapGen;