/**
 * server socket handlers and emitters here. The game API definition.
 */

var UUID = require('node-uuid');

var Tower = require('../public/core/towers/tower');
var Creep = require('../public/core/creeps/creep');
var Game = require('../public/core/Game');
var MapGen = require('../bin/map_gen');
var balance = require('../public/core/balance');

//todo creep.fromjson lives in factory... place it somewhere smarter
require('../public/core/creeps/factory');

var player1 = 0;

// holds all Games. key = room uuid
var games = {};


module.exports = function (io) {
    return function (socket) {
        // Generate a new UUID, looks something like
        // 5b2ca132-64bd-4513-99da-90e838ca47d1
        // and store this on their socket/connection
        socket.userid = UUID();
        var playerIndexes = {};
        console.log('\t socket.io:: player ' + socket.userid + ' connected');
        // tell the player they connected, giving them their id
        socket.emit('onconnected', { id: socket.userid });


        // client emits 'join' event on page load
        socket.on('join', function (callback) {
                if (!player1) {
                    // host the room
                    console.log(socket.userid + ': is waiting for connect');
                    socket.room = socket.userid;
                    socket.join(socket.room);
                    player1 = socket.userid;
                    playerIndexes[socket.userid] = 0;
                    var role = "HOST";
                    var map = {
                        width: 5,
                        height: 10,
                        path: MapGen.generate(10, 15)
                    };

                    games[player1] = new Game.Game();
                    games[player1].state.map = map;
                    var player = {
                        gold: balance.Player.gold,
                        income: balance.Player.income,
                        life: balance.Player.life,
                        id: socket.userid,
                        creeps:[],
                        towers:[],
                        creepTypes: ['CreepFast', 'CreepSlow', 'Creep'],
                        towerTypes: ['Tower','SlowTower']

                    }
                    games[player1].state.players.push(player);
                }
                else {
                    // join available room
                    console.log(socket.userid + ': is joining the waiting player.');
                    socket.join(player1);
                    socket.room = player1;
                    playerIndexes[socket.userid] = 1;
                    var player = {
                        gold: balance.Player.gold,
                        income: balance.Player.income,
                        life: balance.Player.life,
                        id: socket.userid,
                        creeps:[],
                        towers:[],
                        creepTypes: ['CreepFast', 'CreepSlow', 'Creep'],
                        towerTypes: ['Tower']

                    }
                    games[socket.room].state.players.push(player);
                    games[socket.room].state.timeStamp = (new Date()).valueOf();

                    var timeSyncTimer = setInterval(function () {
                        io.sockets.in(socket.room).emit('time', {
                            timeStamp: (new Date()).valueOf(),
                            lastUpdate: games[socket.room].state.timeStamp
                        });

                    }, 2000)

                    player1 = 0;
                    var role = "NON-HOST";
                    io.sockets.in(socket.room).emit('gamestart', games[socket.room].toJson());
                    games[socket.room].updateEvery(100);
                }

                callback({role: role, roomId: socket.room});


            }
        )
        ;

// When this client disconnects
        socket.on('disconnect', function () {
            // Useful to know when someone disconnects
            if (typeof games[socket.room] !== 'undefined') {
                games[socket.room].over();
            }
            console.log('\t socket.io:: client disconnected ' + socket.userid);
        });

        socket.on('tower_buy', function (json) {
            console.log('recv tower_buy')
            var playerId = socket.userid;
            var playerIndex = playerIndexes[playerId];
            var game = games[socket.room];
            var tower = new Tower(json.x, json.y);

            if (game.canAffordTower(tower, playerIndex)) {
                json.playerId = playerId;
                json.playerIndex = playerIndex;
                json.timeStamp = (new Date()).valueOf();
                json.id = game.newId_();
                tower.id = json.id;
                io.sockets.in(socket.room).emit('tower_buy', json);
                game.buyTower(tower, playerIndex, json.timeStamp);
            }
            // console.log('player[' + playerIndex + '] now has ' + gamestate.lands[playerIndex].towers.length + ' towers.');
        });
        socket.on('tower_sell', function (data) {
            var towerAction = JSON.parse(data);
            console.log("tower_sell:" + data);
        });
        socket.on('creep_buy', function (json) {
            var playerId = socket.userid;
            var playerIndex = playerIndexes[playerId];
            var game = games[socket.room];
            var creep = Creep.fromJson(json);

            if (game.canAffordCreep(creep, playerIndex)) {
                json.playerId = playerId;
                json.playerIndex = playerIndex;
                json.timeStamp = (new Date()).valueOf();
                json.id = game.newId_();
                creep.id = json.id;
                io.sockets.in(socket.room).emit('creep_buy', json);
                game.buyCreep(creep, playerIndex, json.timeStamp);
            }

        });


    }
        ;
}
;
