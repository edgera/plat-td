if (typeof require != 'undefined') {
    Tower = require('./towers/tower');
}
(function(exports) {
    /**
     * The game instance that's shared across all clients and the server
     */
    var Game = function() {
        this.state = {};
        this.oldState = {};
        this.state.players = [];

        // Last used ID
        this.lastId = 0;
        this.callbacks = {};

        // Counter for the number of updates
        this.updateCount = 0;
        // Timer for the update loop.
        this.timer = null;
    };

    Game.UPDATE_INTERVAL = Math.round(1000 / 30);
    Game.MAX_DELTA = 10000;
    Game.WIDTH = 640;
    Game.HEIGHT = 480;
    Game.TRANSFER_RATE = 0.05;
    Game.TARGET_LATENCY = 1000; // Maximum latency skew.
    Game.RESTART_DELAY = 1000;

    /**
     * Computes the game state
     * @param {number} delta Number of milliseconds in the future
     * @return {object} The new game state at that timestamp
     */
    Game.prototype.computeState = function(delta) {
        var newState = {
            players : this.state.players,
            objects : {},
            timeStamp: this.state.timeStamp + delta,
            map: this.state.map
        };
        var newObjects = newState.objects;
        var objects = this.state.objects;
        // Generate a new state (movement)
        for (var objId in objects) {
            var obj = objects[objId];
            var newObj = obj.computeState(delta,this);
            if(!newObj.isFinishedOnLand ) {

                newObjects[obj.id] = newObj;
            }
        }
        //TODO DO STUFF LIKE SHOOT. new state(objects interact at new position)

        //economy and bookkeeping
        for (i in newState.players){
            var player = newState.players[i];
            player.gold += player.income * delta;
        }


        return newState;
    };

    /**
     * Computes the game state for a given timestamp in the future
     * @param {number} timeStamp Timestamp to compute for
     */
    Game.prototype.update = function(timeStamp) {
        var delta = timeStamp - this.state.timeStamp;
        if (delta < 0) {
            //throw "Can't compute state in the past. Delta: " + delta;
            delta = 0;
        }
        if (delta > Game.MAX_DELTA) {
            throw "Can't compute state so far in the future. Delta: " + delta;
        }
        this.state = this.computeState(delta);
        this.updateCount++;
    };

    /**
     * Set up an accurate timer in JS
     */
    Game.prototype.updateEvery = function(interval, skew) {
        if (!skew) {
            skew = 0;
        }
        var lastUpdate = (new Date()).valueOf() - skew;
        var ctx = this;
        this.timer = setInterval(function() {
            var date = (new Date()).valueOf() - skew;
            if (date - lastUpdate >= interval) {
                ctx.update(date);
                lastUpdate += interval;
            }
        }, 1);
    };

    Game.prototype.over = function() {
        clearInterval(this.timer);
    };

    /**
     * Called when a new player joins
     */
    Game.prototype.join = function(id) {
        // Add the player to the world
        var player = new Player({
            id: id,
            land:{}
        });
        this.state.objects[player.id] = player;
        return player.id;
    };

    /**
     * Called when a player leaves
     */
    Game.prototype.leave = function(playerId) {
        delete this.state.objects[playerId];
    };


    /**
     * Used by buyer and server to check for valid buy
     * @param {object} info {id, direction, timeStamp}
     */
    Game.prototype.canAffordTower = function(tower,playerIndex) {
        if (tower.cost > this.state.players[playerIndex].gold) {
            return false;
        }
        return true;
    };

    Game.prototype.canAffordCreep = function(creep,playerIndex) {
        if (creep.cost > this.state.players[playerIndex].gold) {
            return false;
        }
        return true;
    };

    /**
     * Called when a player buys
     * @param {object} info {id, direction, timeStamp}
     */
    Game.prototype.buyTower = function(tower,playerIndex,timeStamp) {
        console.log('bought a tower');
        tower.ownerIndex = playerIndex;
        this.state.players[playerIndex].towers.push(tower.id);
        this.state.objects[tower.id]=tower;
        this.state.players[playerIndex].gold -= tower.cost;
        this.state.timeStamp = timeStamp;
    };

    Game.prototype.buyCreep = function(creep,playerIndex,timeStamp) {
        console.log('bought a creep' + creep.id);
        creep.ownerIndex = playerIndex;

        var targetIndex = (playerIndex+1)%this.state.players.length; //pass to next player's land
        creep.currIndex = targetIndex;
        this.state.players[playerIndex].gold -= creep.cost;
        this.state.players[playerIndex].income += creep.income;
        this.state.players[targetIndex].creeps.push(creep.id);

        this.state.objects[creep.id]=creep;
        this.state.timeStamp = timeStamp;
    };


    Game.prototype.getPlayerCount = function() {
        var count = 0;
        var objects = this.state.objects;
        for (var id in objects) {
            if (objects[id].type == 'player') {
                count++;
            }
        }
        return count;
    };

    /***********************************************
     * Loading and saving
     */

    /**
     * Save the game state.
     * @return {object} JSON of the game state
     */
    Game.prototype.toJson =Game.prototype.save = function() {
        var serialized = {
            objects: {},
            timeStamp: this.state.timeStamp,
            players : this.state.players,
            map: this.state.map
        };
        for (var id in this.state.objects) {
            var obj = this.state.objects[id];
            // Serialize to JSON!
            serialized.objects[id] = obj.toJSON();
        }


        return serialized;
    };


    /**
     * Load the game state.
     * @param {object} gameState JSON of the game state
     */
    Game.prototype.load = function(savedState) {
        //console.log(savedState.objects);
        var objects = savedState.objects;
        this.state = {
            objects: {},
            players: savedState.players,
            timeStamp: savedState.timeStamp.valueOf(),
            map:savedState.map
        }
        for (var id in objects) {
            var obj = objects[id];
            // Depending on type, instantiate.
            if (obj.type == 'Tower') {
                t =  new Tower(obj.x,obj.y);
                t.id = obj.id;
                this.state.objects[obj.id] = new Tower(obj.x,obj.y);
            } else if (obj.type == 'player') {
                this.state.objects[obj.id] = new Player(obj);
            }
            // Increment this.lastId
            if (obj.id > this.lastId) {
                this.lastId = obj.id;
            }
        }
    };

    Game.prototype.blobExists = function(blobId) {
        return this.state.objects[blobId] !== undefined;
    };

    /***********************************************
     * Helper functions
     */



    /**
     *
     */
    Game.prototype.callback_ = function(event, data) {
        var callback = this.callbacks[event];
        if (callback) {
            callback(data);
        } else {
            throw "Warning: No callback defined!";
        }
    };

    /**
     * Deterministically generate new ID for an object
     */
    Game.prototype.newId_ = function() {
        return ++this.lastId;
    };

    /**
     *
     */
    Game.prototype.on = function(event, callback) {
        // Sample usage in a client:
        //
        // game.on('dead', function(data) {
        //   if (data.id == player.id) {
        //     // Darn -- player died!
        //   }
        // });
        this.callbacks[event] = callback;
    };

    /**
     * Instance of a blob in the world
     */
    var Blob = function(params) {
        if (!params) {
            return;
        }
        this.id = params.id;
        this.x = params.x;
        this.y = params.y;
        this.r = params.r;
        this.vx = params.vx;
        this.vy = params.vy;
        if (!this.type) {
            this.type = 'blob';
        }
    };


    /**
     * Gives the amount of overlap between blobs (assuming blob and this are
     * overlapping, and that blob < this.
     * @returns {number} Amount of overlap
     */
    Blob.prototype.overlap = function(blob) {
        var overlap = blob.r + this.r - this.distanceFrom(blob);
        return (overlap > 0 ? overlap : 0);
    };

    Blob.prototype.intersects = function(blob) {
        return this.distanceFrom(blob) < blob.r + this.r;
    };

    Blob.prototype.distanceFrom = function(blob) {
        return Math.sqrt(Math.pow(this.x - blob.x, 2) + Math.pow(this.y - blob.y, 2));
    };

    Blob.prototype.area = function() {
        return Math.PI * this.r * this.r;
    };

    /**
     * Transfers some area to (or from if area < 0) this blob.
     */
    Blob.prototype.transferArea = function(area) {
        var sign = 1;
        if (area < 0) {
            sign = -1;
        }
        this.r += sign * Math.sqrt(Math.abs(area) / Math.PI);
    };

    /**
     * Create a new state for this blob in the future
     */
    Blob.prototype.computeState = function(delta) {
        // TODO: dampen vx and vy slightly?
        var newBlob = new this.constructor(this.toJSON());
        newBlob.x += this.vx * delta/10;
        newBlob.y += this.vy * delta/10;
        return newBlob;
    };

    Blob.prototype.toJSON = function() {
        var obj = {};
        for (var prop in this) {
            if (this.hasOwnProperty(prop)) {
                obj[prop] = this[prop];
            }
        }
        return obj;
    };

    /**
     * Instance of a player (a kind of blob)
     */
    var Player = function(params) {
        this.name = params.name;
        this.type = 'player';

        Blob.call(this, params);
    };

    Player.prototype = new Blob();
    Player.prototype.constructor = Player;

    exports.Game = Game;
    exports.Player = Player;
    exports.Blob = Blob;

})(typeof global === "undefined" ? window : exports);
