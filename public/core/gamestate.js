
//if this gamestate.js file is being used by nodejs server
if (typeof require != 'undefined'){
    var Land = require('./land');
}


function Gamestate(players, factions, map){
    if(arguments.length == 0){
        this.players = []; // array of player ids
        this.lands = {} ; // map of player->land object
        this.map = {} ;
    }
    if(arguments.length == 3){
        this.players = players; // array of player ids
        this.lands = {};// map of player->land object
        this.factions = factions;
        for (var i = 0; i<players.length; i++) {
            var player = players[i];
            var land = new Land(player,factions[i].creepTypes);
            this.lands[player] = land;
        }
        this.map = map;
    }
};
Gamestate.prototype.clear = function(){
    for(var i = 0; i < this.players.length; i++){
        var player = this.players[i];
        this.lands[player].clear();
    }
};

Gamestate.prototype.toJson = function(){
    var json_gamestate = {   };
    json_gamestate['lands']={};
    json_gamestate['map'] = this.map;
    json_gamestate['players']=this.players;
    json_gamestate['factions']=this.factions;
    json_gamestate['tick']=this.tick;
    for(var i = 0; i < this.players.length; i++){
        var player = this.players[i];
        json_gamestate.lands[player] = this.lands[player].toJson();
    }

    return json_gamestate;
};

Gamestate.prototype.copy = function(){
    var gamestate = new Gamestate();
    gamestate.map= this.map;
    gamestate.players = this.players;
    gamestate.lands = {};
    for (var i = 0; i<this.players.length; i++) {
        var player = this.players[i];
        gamestate.lands[player] = this.lands[player].copy();
    }
    gamestate.tick = this.tick;
    return gamestate;

};





Gamestate.fromJson = function(json_gamestate){
    var map = json_gamestate['map'];
    var players = json_gamestate['players'];

    var gamestate = new Gamestate();
    gamestate.map = map;
    gamestate.players = [];
    gamestate.lands = {};

    for (var i = 0; i<players.length; i++) {
        var player = players[i];
        gamestate.players.push(player);
        var json_land = json_gamestate.lands[player];
        gamestate.lands[player] = Land.fromJson(json_land);
    }
    gamestate.tick = json_gamestate['tick'];
    return gamestate;
};

module.exports = Gamestate;