var balance = Object.freeze(
{
    // PLAYER BASE STATS
    Player: {
        life: 10,
        income:.1,
        gold: 400,
        towers:[],
        creeps:[]
    },

    //CREEPS
    Creep: {
        speed:.002,
        health: 100,
        attack_damage: 1,
        cost: 100,
        income: 50
    },
    CreepSlow: {
        speed: .15,
        health: 100,
        attack_damage: 1,
        cost: 100,
        income: 50
    },
    CreepFast: {
        speed: .5,
        health: 100,
        attack_damage: 1,
        cost: 100,
        income: 50
    },
    //TOWERS
    Tower:{
        range : 2,
        charge : 100,
        cost :50,
        attack_damage:50,
        cooldown:0
    }


})

module.exports = balance;