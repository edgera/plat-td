if (typeof require != 'undefined'){
    var CreepEnum = require('./creeps/factory');
    var Tower = require('./towers/tower');
}

function Land(player, creepTypes, towerTypes) {
    this.player = player;
    this.creepTypes = creepTypes;

    //active stuff
    this.life = 10;
    this.gold = 200;
    this.income = 0;
    this.creeps = [];
    this.towers = [];
}


Land.prototype.toJson = function () {
    var json =
    {
        player: this.player,
        income: this.income,
        life: this.life,
        gold: this.gold,
        towers:[],
        creeps:[],
        creepTypes: this.creepTypes
    };

    for(var i = 0; i < this.creeps.length; i ++){
        json.creeps.push(this.creeps[i].toJson());
    }
    for(var i = 0; i < this.towers.length; i ++){
        json.towers.push(this.towers[i].toJson());
    }

    return json;
};


Land.prototype.clear = function(){
    for(var j = 0; j < this.creeps.length; j ++){
        var obj = this.creeps[j];
        if(obj.sprite ){
            obj.sprite.graphics.c();
        }
    }
    for(var j = 0; j < this.towers.length; j ++){
        var obj = this.towers[j];
        obj.sprite.graphics.c();
    }
};

Land.prototype.copy = function(){
    var land = new Land(this.player,this.creepTypes);

    land.income = this.income;
    land.life = this.life;
    land.gold = this.gold;
    land.creeps = [];
    land.towers =[];
    for(var j = 0; j < this.creeps.length; j ++){
        land.creeps.push(this.creeps[i].copy());
    }
    for(var j = 0; j < this.towers.length; j ++){
        land.towers.push(this.towers[i].copy());
    }
     return land;
}

Land.fromJson = function(json){
 //   var json = json_gamestate.lands[player];
    var land = new Land(json.player,json.creepTypes);

    land.income = json.income;
    land.life = json.life;
    land.gold = json.gold;


    var creeps_json =   json.creeps;
    for(var j = 0; j < creeps_json.length; j ++){
        var obj = creeps_json[j];
        var creep = new Creep.fromJson(obj,stage);
        creep.suspend_timer=j*50; //TODO variable creep spawn rate
        land.creeps.push(creep);
    }
    var towers_json =  json.towers;

    for(var j = 0; j < towers_json.length; j ++){
        var obj = towers_json[j];
        var tower = new Tower(obj.x,obj.y);
        land.towers.push(tower);
    }

    return land;
}

module.exports = Land;