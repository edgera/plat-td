/**
 * Created by edgera on 9/30/2014.
 */
// potential creeps
var TowerEnum = Object.freeze(
    {
        GENERIC: Tower,
        RED: 1,
        BLUE: 2
    }
);


// Constructor
function Tower(x,y) {
    this.cooldown = balance[this.constructor.name].cooldown;  //ticks between shots
    this.charge = balance[this.constructor.name].charge;
    this.range = balance[this.constructor.name].range;
    this.attack_damage = balance[this.constructor.name].attack_damage;
    this.cost = balance[this.constructor.name].cost;

    //relative to land
    this.x = x;
    this.y = y;
}



// class methods
Tower.prototype.updateSprite = function() {
    //dont need to do anything yet.
}
Tower.prototype.create_sprite = function(stage, playerIndex) {
    this.stage = stage;
    var circle = new createjs.Shape();
    circle.graphics.beginFill("red").drawCircle(0, 0, grid_height/2);
    circle.x = Math.round((this.x * grid_width) +(playerIndex*grid_width*row_length));
    circle.y = Math.round(this.y * grid_height);

    //circle.addEventListener("click", function(e) { alert("some cool tower"); });
    stage.addChild(circle);
    stage.update();
    this.sprite = circle;

    return circle;
};

Tower.prototype.computeState = function(delta){
    if(this.cooldown>0) {
        this.cooldown -= 1;
    }
    return this;
}

//@args array of creep objects
Tower.prototype.shootAt = function(creeps){
    //TODO real targeting
    for(var i = 0 ; i < creeps.length;i++){
        var x2 =(creeps[i].x - this.x)*(creeps[i].x - this.x);
        var y2 =(creeps[i].y - this.y)*(creeps[i].y - this.y);

        if(x2+y2 < this.range*this.range &&  this.cooldown == 0 &&creeps[i].health>0){
            // found target.
            // todo just remove health for now. in future do different affects.
            creeps[i].health -= 50;
            this.cooldown = this.charge;
            console.log('tower fired shot');
            if (typeof this.sprite != 'undefined') {
                this.animateShot();
            }

            break;
        }
    }

    if(this.cooldown>0) {
        this.cooldown -= 1;
    }
};


//bad hacky show active towers
Tower.prototype.animateShot = function(){
    if(this.sprite == null){
        return;
    }
    var _this = this;
    _this.sprite.graphics.c();//tiny clear

    this.sprite.graphics.beginFill("#FF00A0").drawCircle(0, 0, grid_width/2+5);
    this.stage.update();
    setTimeout(function () {
        _this.sprite.graphics.c();//tiny clear
        _this.sprite.graphics.beginFill("red").drawCircle(0, 0, grid_width/2);
        _this.stage.update();
    }, 5*1000/target_tps);
}


Tower.prototype.toJson = function(){

    var json = {
        x:this.x,
        y:this.y,
        type:this.constructor.name,
        range:this.range,
        charge:this.charge
    };
    return json;
}


Tower.prototype.copy = function(){
    var tower = new Tower(this.x,this.y);
    tower.charge = this.charge;
    tower.range = this.range;
    tower.type = this.constructor.name;
    return tower;
}


// export the class
module.exports = Tower;