/**
 * Created by edgera on 11/22/2014.
 */
if (typeof require != 'undefined') {
    Tower = require('./tower');
//    TowerMachinegun = require('./tower_machinegun');
//    TowerLaser = require('./tower_laser');
}

var TowerEnum = Object.freeze(
    {
        Tower: Tower/*,
        TowerMachinegun: TowerMachinegun,
        TowerLaser: TowerLaser*/
    }
);



Tower.fromJson = function(k,json){
    console.log(json);
    return new TowerEnum[json.type](json.x,json.y);
};



module.exports = TowerEnum;