/**
 * Created by edgera on 11/22/2014.
 */
if (typeof require != 'undefined') {
    Creep = require('./creep');
    CreepFast = require('./creep_fast');
    CreepSlow = require('./creep_slow');
}

var CreepEnum = Object.freeze(
    {
        Creep: Creep,
        CreepFast: CreepFast,
        CreepSlow: CreepSlow
    }
);


//TODO this isn't done yet.
Creep.fromJson = function(json,stage){
    var creep = new CreepEnum[json.type](stage);
    creep.travel_distance = json.travel_distance;
    creep.health = json.health;
    return creep;
}




module.exports = CreepEnum;