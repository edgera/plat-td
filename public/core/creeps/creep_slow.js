/**
 * Created by edgera on 11/22/2014.
 */
if (typeof require != 'undefined') {
    Creep = require('./creep');
}

function CreepSlow(stage) {
    Creep.call(this,stage);

}

CreepSlow.prototype = Object.create(Creep.prototype);
CreepSlow.prototype.constructor = CreepSlow;


CreepSlow.prototype.create_sprite = function(stage,target_land_index) {
    this.stage = stage;

    var circle = new createjs.Shape();
    circle.graphics.beginFill("blue").drawCircle(0, 0, grid_height/2);
    circle.x = this.x * grid_width + (target_land_index*grid_width *row_length);
    circle.y = this.y * grid_height;
    stage.addChild(circle);
    this.sprite = circle;
    return circle;
};


module.exports = CreepSlow;

//Creep.prototype.updateDelta = function(ticks,path) {};


