/**
 * Created by edgera on 11/22/2014.
 */
if (typeof require != 'undefined') {
    Creep = require('./creep');
}

function CreepFast() {
    Creep.call(this);

}

CreepFast.prototype = Object.create(Creep.prototype);
CreepFast.prototype.constructor = CreepFast;


CreepFast.prototype.create_sprite = function (stage, target_land_index) {
    this.stage = stage;

    var imgMonsterARun = new Image();
    imgMonsterARun.src = "images/sprites.png";
    var spriteSheet = new createjs.SpriteSheet({
        // image to use
        images: [imgMonsterARun],
        // width, height & registration point of each sprite
        frames: {width: 16, height: 16, regX: 16, regY: 16,count:20},
        animations: {
            walk: [0, "walk"]
        }
    });
    this.sprite = new createjs.Sprite(spriteSheet);
    this.sprite.gotoAndPlay("walk");
    this.sprite.x = this.x * grid_width + (target_land_index * grid_width * row_length);
    this.sprite.y = this.y * grid_height;
    stage.addChild( this.sprite);

//    var circle = new createjs.Shape();
//    circle.graphics.beginFill("blue").drawCircle(0, 0, grid_height / 2);
//    circle.x = this.x * grid_width + (target_land_index * grid_width * row_length);
//    circle.y = this.y * grid_height;
//    stage.addChild(circle);
//    this.sprite = circle;

    return this.sprite ;
};

module.exports = CreepFast;
//Creep.prototype.updateDelta = function(ticks,path) {};


