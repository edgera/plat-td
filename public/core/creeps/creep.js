/**
 * Created by edgera on 9/30/2014.
 */


if (typeof require != 'undefined') {

    balance = require('../balance');
}



// Constructor
function Creep() {
    //required
    this.speed = balance[this.constructor.name].speed;
    this.health = balance[this.constructor.name].health;
    this.attack_damage = balance[this.constructor.name].attack_damage;
    this.cost = balance[this.constructor.name].cost;
    this.income = balance[this.constructor.name].income;

    this.currIndex = null;
    this.ownerIndex = null;

    this.x = -100;
    this.y = -100;
    this.travel_distance = 0;
    this.suspend_timer = 0;
    this.isFinishedOnLand = false


}
// class methods
Creep.prototype.create_sprite = function(stage, target_land_index) {
    this.stage = stage;

    var circle = new createjs.Shape();
    circle.graphics.beginFill("blue").drawCircle(0, 0, grid_height/2);
    circle.x = this.x * grid_width + (target_land_index*grid_width *row_length);
    circle.y = this.y * grid_height;
    stage.addChild(circle);
    this.sprite = circle;
    return circle;
};



Creep.prototype.removeSprite = function(){
    if(this.stage){
        this.stage.removeChild(this.sprite);
        console.log('remove child!')
    }
};

Creep.prototype.updateSprite = function(){
    if (typeof this.sprite != 'undefined') {
        this.sprite.x = Math.round(this.x * grid_width + (this.currIndex *grid_width *row_length));
        this.sprite.y = Math.round(this.y * grid_height);
    }
}

Creep.prototype.computeState = function(delta,game){
    var path= game.state.map.path;
    this.travel_distance += delta * this.speed;

    //figure out how far along path the creep has gotten.
    var potential_travel = 0, traveled = 0;
    var x0, y0, x1, y1;

    for (var i = 0; i < path.length - 3; i += 2) {
        x0 = path[i];
        y0 = path[i + 1];
        x1 = path[i + 2]
        y1 = path[i + 3];

        potential_travel = Math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));
        if (traveled + potential_travel > this.travel_distance) {
            break;
        }
        traveled += potential_travel;
    }
    //check if creep has left the path.
    if (i + 2 >= path.length) {
        this.travel_distance = 0;
        this.currIndex = (this.currIndex + 1)%game.state.players.length;
        if(this.currIndex == this.ownerIndex ){
            this.isFinishedOnLand = true;
        }

        return this; //dont draw.
    }

    //#usetrig
    var residual_distance = this.travel_distance - traveled;
    var ratio = residual_distance / potential_travel;

    this.x = x0 + (ratio * (x1 - x0));
    this.y = y0 + (ratio * (y1 - y0));
    return this;

};

// we only need to know the creep's type to send it on the network.
Creep.prototype.toJson = function(){
    return {
        type:this.constructor.name,
        travel_distance:this.travel_distance,
        health:this.health
    };
}

//The fromJson() function is in Factory for circular dependency issues.

// export the class
module.exports = Creep;