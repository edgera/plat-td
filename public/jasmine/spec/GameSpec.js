describe("GameTest", function () {
    var game;

    beforeEach(function () {
        game = new Game();
    });

    it("should be able to compute and update when given some time delta", function () {
        var origState = game.state;

        expect(origState).toEqual(game.state);
        var newState = game.computeState(100);
        expect(origState).not.toEqual(newState);
    });

    it("should be able to update itself when given some time delta", function () {
        var origState = game.state;

        expect(origState).toEqual(game.state);
        game.update(100);
        expect(origState).not.toEqual(game.state);
    });


});
