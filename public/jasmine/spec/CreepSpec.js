describe("CreepTest", function() {
  var creep;

  beforeEach(function() {
    creep = new Creep();
  });

  it("should be able to be serialized to json", function() {
    var expectedJson = { "type" : "GENERIC" }

    expect(creep.toJson()).toEqual(expectedJson);
  });
});
