//client globals
my_id = null;
game = null;
clientSocket = null;
totalSkew =0;


function clientInit() {

    clientSocket = io.connect('/');

    clientSocket.on('onconnected', function (data) {
        // Note that the data is the object we sent from the server, as is. So we can assume its id exists.
        console.log('Connected successfully to the socket.io server. My server side ID is ' + data.id);
        my_id = data.id;
    });

    clientSocket.emit('join', function (response) {
        console.log('Joined room ' + response.roomId + ' as ' + response.role);
    });

    clientSocket.on('gamestart', function (json) {
        console.log(json);
        game = new Game();
        game.load(json);
        var startDelta = new Date().valueOf() - json.timeStamp;
        game.updateEvery(Game.UPDATE_INTERVAL, startDelta);
        renderer = new Renderer(game);
        renderer.render();
        //init_each_land_visual(game);
    });


    clientSocket.on('sync', function(json){
        game.load(json.state);
    });

    // Get a time sync from the server
    clientSocket.on('time', function (data) {
        if(game == null){
            return;
        }
        // Compute how much we've skewed from the server since the last tick.
        //console.log('got time');
        var updateDelta = data.lastUpdate - game.state.timeStamp;
        // Add to the cumulative skew offset.
        totalSkew += updateDelta;
        // If the skew offset is too large in either direction, get the real state
        // from the server.
        if (Math.abs(totalSkew) > Game.TARGET_LATENCY) {
        // Fetch the new truth from the server.
            clientSocket.emit('state');
            totalSkew = 0;
        }
        // Set the true timestamp anyway now.
        //game.state.timeStamp = data.lastUpdate;
        // Number of clients that aren't playing.
       // document.getElementById('player-count').innerText = game.getPlayerCount();
        document.getElementById('average-lag').innerHTML = 'lag:'+Math.abs(updateDelta);
    });


    /* swag */

    clientSocket.on('tower_buy', function(json){
        var tower = new Tower(json.x, json.y);
        tower.id = json.id;
        game.buyTower(tower,json.playerIndex,json.timeStamp);
    });


    clientSocket.on('creep_buy', function(json){
        var creep = Creep.fromJson(json);
        creep.id = json.id;
        game.buyCreep(creep,json.playerIndex,json.timeStamp);
    });


}
