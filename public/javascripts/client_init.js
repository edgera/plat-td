/**
 * client side initialization
 */


var my_id;
var gamestate;
var clientSocket;
var tick =0;


// called on page load
// initialize global vars and attach client's socket event listeners
function clientInit() {
    clientSocket = io.connect('/');
    attachClientSocketEvents();
}

// attach event listeners to socket events

var target_tps = 20,
    target_delay = 1000/target_tps,
    time = 0;

//http://www.sitepoint.com/creating-accurate-timers-in-javascript/

function start_ticking(){
    Engine.tick(gamestate);
    tick++;
    update_stats_display(gamestate);
    buff.push({gamestate:gamestate.copy(),actions:[]});
    //TODO check if game is done.
    time+=target_delay;
    // elapsed = Math.floor(time/target_tps)/10
    var diff = (new Date().getTime()-start)-time;

    setTimeout(function () {
        start_ticking();
    }, target_delay - diff);
    document.getElementById('tick').innerHTML = tick + '';;
}

