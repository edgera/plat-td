/**
 * put client socket event handlers here
 */

function do_wave(json_gamestate){
    console.log('doing a wave');
//    stage.mouseEnabled = false;
//    stage.mouseChildren = false;
    // 1 lock input until the end of the wave. TODO
    // 2 convert the gamestate json representation to full objects.
    gamestate.clear();
    gamestate = Gamestate.fromJson(json_gamestate);
    // 2a add all of the objects to the appropriate stages.
    // 2b remove all invalid towers (only happens if player manually added by modifying clientside js)
    draw_wave_updates();

    // 3 process each tick of the wave. We need to do a tick-based calculation
    //   to ensure that the client and server results match, ie a time-based process
    //   would potentially result in different creeps being targeted because of how
    //   the wave distro is made discrete at different timesteps.
    // 3a becaues we do not want to render the creep wves as fast as we can compute them,
    //    we wil use a interupt timer to update the stages every t seconds.
    run_wave();

    // we need to wait for run_wave to complete before executing checks.
    // run_wave() will be responsible for calling checks when it is complete.

};

function draw_wave_updates(){
    for(var i = 0 ; i < gamestate.players.length; i++ ){
        var player= gamestate.players[i];
        var land = gamestate.lands[player];
        // make sure all tower sprites are rendered.
        //TODO dont recreate sprites if
        for(var j = 0; j<land.towers.length; j++){
            var tower = land.towers[j];

            if (tower.sprite == null){
                tower.create_sprite(stage,i);
            }
        }
        for(var j = 0; j<land.creeps.length; j++){
            var creep = land.creeps[j];
            if (creep.sprite == null) {
                creep.create_sprite(stage, i);
            }
        }

        stage.update();
    }
}



function end_wave(gamestate){
    console.log('finished doing a wave');
    Engine.do_end_of_wave(gamestate);
    update_stats_display(gamestate);
//    stage.mouseEnabled = true;
//    stage.mouseChildren = true;

    //check if the game is over
    if(Engine.numberOfRemainingLands(gamestate)==1){
        alert('game end');
    }

}

function update_stats_display(gamestate){
    for(var i = 0 ; i < gamestate.players.length; i++ ){
        var player= gamestate.players[i];

        updateInfoBar(gamestate.lands[player]);
        stage.update();
    }
}


//function buyCreep(creepType) {
//    var creep = new CreepEnum[creepType](stage);
//
//    if(!Engine.buy_creep(creep, my_id, gamestate)){
//        console.log('client-side creep not purchased');
//        return; // TODO visual indicator
//    }
//    updateInfoBar(gamestate.lands[my_id]);
//    clientSocket.emit('creep_buy',creep.toJson() );
//}



//function buyTower(x,y,stage, index) {
//    console.log('buy tower pressed');
//    var tower = new Tower(x, y);
//    clientSocket.emit('tower_buy', tower.toJson());
//}
//




//send TODO move this funcitonality into the server in a timed loop.
function send() {
    console.log('clicked send');
    clientSocket.emit('send', function () {
        console.log('clientsendd clicked callback');
    });
};



