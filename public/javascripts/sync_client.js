/**
 * Created by edgera on 12/3/2014.
 */


function CircularFifoQueue(n) {
    this._array= new Array(n);
    this.oldest = 0;
}
CircularFifoQueue.prototype.push= function(item) {
    this._array[ this.oldest] = item;
    this.oldest =  (this.oldest +1) % this._array.length;
};
CircularFifoQueue.prototype.forgetAfter= function(tick) {
    for (var i = 0; i < this._array.length; i ++){
        //only operate on valid entries
        if(typeof this._array[i] == undefined){
            continue;
        }
        //erase it
        if(this._array[i].gamestate.tick > tick){
            this._array[i] = undefined;
        }
    }
};


CircularFifoQueue.prototype.applyHistory= function(action) {
    //find the tick index
    var latestGoodI =-1;
    for (var i = 0; i < this._array.length; i ++){
        if (typeof this._array[i] !== 'undefined'){
            console.log(action.tick + ' vs ' + this._array[i].gamestate.tick);
        }
        if ((typeof this._array[i] !== 'undefined')
            && (this._array[i].gamestate.tick ==action.tick)){
            latestGoodI =i;
            break;
        }
    }
    if(latestGoodI== -1 ){
            throw {};
    }

    var latestGoodGamestate = this._array[latestGoodI].gamestate;
    for( var i = 0 ; i < this._array.length ; i++){
        var wrapI =( latestGoodI +i )%   this._array.length

        //TODO do new action for this and all subsequent actions.
        this._array[wrapI].gamestate= EnactAction[action.type](gamestate);
    }

};