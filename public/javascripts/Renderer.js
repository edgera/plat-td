
(function(exports) {
// shim layer with setTimeout fallback
    window.requestAnimFrame = (function(){
        return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            function(/* function */ callback, /* DOMElement */ element){
                window.setTimeout(callback, 1000 / 10);
            };
    })();
    //globals
     grid_width = 30;
     grid_height = 30;
     row_length = 10;
     col_length = 15;

    var stage = null;
    var selectedTowerType = null;

    var Renderer = function(game) {
        this.game = game;
        this.initDom();
        this.initMap();
        this.drawCreepSelector();
        this.drawTowerSelector();
        this.spriteMap = {};

//        this.canvas = document.getElementById('canvas');
//        this.context = this.canvas.getContext('2d');
    };

    Renderer.prototype.initDom = function(){
        var game_container = document.getElementById('gameContainer');
        game_container.style.width=game.state.players.length*row_length*grid_width+'px';

        var map_div = document.getElementById('maps');
        var c = document.createElement('canvas');
        c.setAttribute('id', 'theCanvas');
        c.setAttribute('width', this.game.state.players.length * row_length * grid_width);
        c.setAttribute('height', col_length * grid_height + 100);
        map_div.appendChild(c);

        stage = new createjs.Stage("theCanvas");
        stage.enableMouseOver();

        var info_bar_div = document.getElementById('infoBar');
        var selector_bar_div = document.getElementById('selectorBar');
        for (var i = 0; i < game.state.players.length; i++) {
            var player =  game.state.players[i];
            if (player.id == my_id) {
                my_index = i;
            }
            var c = this.createInfoBarBox(game.state.players[i]);
            info_bar_div.appendChild(c);

            var sbox = this.createSelectorBarBox(game.state.players[i]);
            selector_bar_div.appendChild(sbox);
        }
    };

    Renderer.prototype.createInfoBarBox = function(player){
        var box = document.createElement('div');
        box.setAttribute('id', player + '_info');
        box.setAttribute('class', 'infoBarBox ');
        box.setAttribute('style',
                "width:" + row_length * grid_width + "px;" +
                "min-width:" + row_length * grid_width + "px;" +
                "height:100px;"
        );
        var playerDiv = document.createElement('div');
        playerDiv.setAttribute('id',player.id+"_id");
        var health = document.createElement('div');
        health.setAttribute('id',player.id+"_health");
        var income = document.createElement('div');
        income.setAttribute('id',player.id+"_income");
        var gold = document.createElement('div');
        gold.setAttribute('id',player.id+"_gold");


        playerDiv.innerHTML = 'player:' + player.id;
        health.innerHTML = 'hp:' + player.life;
        gold.innerHTML = 'gold:' + player.gold;
        income.innerHTML = 'income:' + player.income;

        box.appendChild(playerDiv);
        box.appendChild(health);
        box.appendChild(income);
        box.appendChild(gold);
        return box;
    }

    Renderer.prototype.createSelectorBarBox = function(player){
        var box = document.createElement('div');
        box.setAttribute('id', player.id + '_selector');
        box.setAttribute('class', 'selectorBarBox ');
        box.setAttribute('style',
                "width:" + row_length * grid_width + "px;" +
                "min-width:" + row_length * grid_width + "px;" +
                "height:100px;"
        );
        box.innerHTML = '&nbsp;';

        return box;
    }

    Renderer.prototype.initMap = function(){
        var info_bar_div = document.getElementById('infoBar');
        for (var i = 0; i < game.state.players.length; i++) {
            this.create_bg(i, my_index == i);
            this.draw_path(i, game.state.map['path']);
        }
    };

    Renderer.prototype.create_bg = function(index, active) {
        for (var i = 0; i < col_length * row_length; i++) {
            var s = new createjs.Shape();
            s.overColor = "#997777";
            s.outColor = "#443333";
            s.pathColor = "#cc9977";
            s.graphics.beginFill(s.outColor).drawRect(0, 0, grid_width, grid_height).endFill();
            s.x = ((grid_width ) * (i % row_length)) + (index * row_length * grid_width);
            s.y = (grid_height ) * (i / row_length | 0);

            if (active) {
                s.on('mouseover', function (event) {
                    var target = event.target;
                    target.graphics.clear().beginFill(target.overColor).drawRect(0, 0, grid_width, grid_height).endFill();
                    stage.update();
                });
                s.on('mouseout', function (event) {
                    var target = event.target;
                    target.graphics.clear().beginFill(target.outColor).drawRect(0, 0, grid_width, grid_height).endFill();
                    stage.update();
                });
                s.on('mousedown', function (e) {
                        if(selectedTowerType != null){
                            console.log('tower buy... type:' + selectedTowerType);
                            var x = Math.floor((e.stageX -(my_index*grid_width*row_length))/ grid_width )  +.5;
                            var y = Math.floor(e.stageY / grid_width)  +.5;
                            clientSocket.emit('tower_buy',{x:x,y:y});
                            selectedTowerType = null;
                            document.getElementById(my_id+'_selector').innerHTML = '&nbsp;';

                        }

                    }
                );
            }
           stage.addChild(s);
        }

    };

    Renderer.prototype.drawCreepSelector = function () {
        var creepTypes  = game.state.players[my_index].creepTypes;
        var playerId = game.state.players[(my_index + 1)%game.state.players.length].id;
        console.log(playerId);
        for (var i = 0; i < creepTypes.length; i++) {
            var s = new createjs.Shape();
            s.overColor = "#123456";
            s.outColor = "#123456";
            s.pathColor = "#cc9977";
            s.graphics.beginFill(s.overColor).drawRect(0, 0, grid_width, grid_height).endFill();
            s.x = grid_width * i + grid_width + (((my_index+1)%game.state.players.length) * grid_width*row_length);
            s.y = (grid_height * col_length) + grid_height;
            console.log(creepTypes);
            s.creepType = creepTypes[i];

            s.on('mouseover', function (event) {
                var target = event.target;
                document.getElementById(playerId+'_selector').innerHTML = target.creepType;

            });
            s.on('mouseout', function (event) {
                var target = event.target;
                document.getElementById(playerId+'_selector').innerHTML = '&nbsp;';
            });

            s.on('mousedown', function (event) {
                var target = event.target;
                console.log('creating creep of type:'+target.creepType);
                // clientSocket.emit('tower_buy',{x:x,y:y});
                var creep = new CreepEnum[target.creepType](stage);
//                if(!Engine.buy_creep(creep, my_id, gamestate)){
//                    console.log('client-side creep not purchased');
//                    return; // TODO check price
//                }
                clientSocket.emit('creep_buy',creep.toJson() );

            });

            stage.addChild(s);

        }
        stage.update();

    }

    Renderer.prototype.drawTowerSelector = function () {
        var towerTypes  = game.state.players[my_index].towerTypes;


        for (var i = 0; i < towerTypes.length; i++) {
            var s = new createjs.Shape();
            s.overColor = "#123456";
            s.outColor = "#123456";
            s.pathColor = "#cc9977";
            s.graphics.beginFill(s.overColor).drawRect(0, 0, grid_width, grid_height).endFill();
            s.x = grid_width * i + grid_width + (my_index * grid_width*row_length);
            s.y = (grid_height * col_length) + grid_height;
            s.towerType = towerTypes[i];

            s.on('mouseover', function (event) {
                var target = event.target;
                document.getElementById(my_id+'_selector').innerHTML = target.towerType;

            });
            s.on('mouseout', function (event) {
                var target = event.target;
                if (selectedTowerType == null) {
                    document.getElementById(my_id + '_selector').innerHTML = '&nbsp;'
                }
            });

            s.on('mousedown', function (event) {
                var target = event.target;
                console.log('selecting tower type:'+target.towerType);
               // var creep = new CreepEnum[target.creepType](stage);
//                if(!Engine.buy_creep(creep, my_id, gamestate)){
//                    console.log('client-side creep not purchased');
//                    return; // TODO check price
//                }
                //clientSocket.emit('creep_buy',creep.toJson() );
                selectedTowerType = target.towerType;
                document.getElementById(my_id+'_selector').innerHTML = '<b>'+target.towerType+'</b>';

            });

            stage.addChild(s);

        }
        stage.update();

    }




    Renderer.prototype.draw_path = function draw_path(index, path) {

        for (var i = 0; i < path.length - 3; i += 2) {
            x0 = path[i] * grid_width + (index * row_length * grid_width);
            y0 = path[i + 1] * grid_height;
            x1 = path[i + 2] * grid_width + (index * row_length * grid_width);
            y1 = path[i + 3] * grid_height;
            var line = new createjs.Shape();
            line.graphics.moveTo(x0, y0).setStrokeStyle(1).beginStroke("#00ff00").lineTo(x1, y1);
           stage.addChild(line);
        }
       stage.update();
    };


    Renderer.prototype.updateInfoBar = function() {
        for (var i = 0; i < game.state.players.length; i++) {
            player = game.state.players[i];
            document.getElementById(player.id+'_health').innerHTML = 'hp:' +  player.life;
            document.getElementById(player.id+'_gold').innerHTML = 'gold:' +  player.gold;
            document.getElementById(player.id+'_income').innerHTML = 'income:' +  player.income;
        }
    }

    Renderer.prototype.render = function() {

        //this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        var objects = this.game.state.objects;


        //if the object no longer exists, remove it from our sprites
        for (var i in this.spriteMap){
            var o = this.spriteMap[i];
            if(!objects[o.id]){
                console.log('clearing gfx for ' + o.id)
                this.spriteMap[o.id].removeSprite();
                //this.stage.removeChild( this.spriteMap[o.id].sprite);
                //this.spriteMap[o.id].sprite.c();
                delete this.spriteMap[o.id];
            }
        }

        // Render the game state
        for (var i in objects) {
            var o = objects[i];
            this.renderObject_(o);
        }
        //update the dom
       // this.updateInfoBar();

        stage.update();
        var ctx = this;
        requestAnimFrame(function() {
            ctx.render.call(ctx);
        });
    };

    Renderer.prototype.renderObject_ = function(obj) {
        if(!this.spriteMap[obj.id]){
            //console.log('createing sprite');
            obj.create_sprite(stage,obj.ownerIndex);
            this.spriteMap[obj.id]=obj;
        }
        obj.updateSprite();
    };

    exports.Renderer = Renderer;

})(window);
