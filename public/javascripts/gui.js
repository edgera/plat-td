/**
 * Created by edgera on 11/22/2014.
 */


var stage, canvas;

if (typeof require != 'undefined') {
    CreepFactory = require('../core/creeps/factory');
}

var grid_width = 30;
var grid_height = 30;
var row_length = 10;
var col_length = 15;
var my_index;



/*
 * The intention of this function is to create each player's game board and display it
 * - creates the canvas,
 * - places it in the DOM,
 * - creates the interactions on the background,
 * - draws the path
 */
function createInfoBarBox(player) {
    var box = document.createElement('div');
    box.setAttribute('id', player + '_info');
    box.setAttribute('class', 'infoBarBox ');
    box.setAttribute('style',
            "width:" + row_length * grid_width + "px;" +
            "min-width:" + row_length * grid_width + "px;" +
            "height:100px;"
    );
    var playerDiv = document.createElement('div');
    playerDiv.setAttribute('id',player.id+"_id");
    var health = document.createElement('div');
    health.setAttribute('id',player.id+"_health");
    var income = document.createElement('div');
    income.setAttribute('id',player.id+"_income");
    var gold = document.createElement('div');
    gold.setAttribute('id',player.id+"_gold");


    playerDiv.innerHTML = 'player:' + player.id;
    health.innerHTML = 'hp:' + player.life;
    gold.innerHTML = 'gold:' + player.gold;
    income.innerHTML = 'income:' + player.income;

    box.appendChild(playerDiv);
    box.appendChild(health);
    box.appendChild(income);
    box.appendChild(gold);
    return box;
}



function updateInfoBar(land) {
    var player = land.player;
    document.getElementById(player+'_health').innerHTML = 'hp:' +  land.life;
    document.getElementById(player+'_gold').innerHTML = 'gold:' +  land.gold;
    document.getElementById(player+'_income').innerHTML = 'income:' +  land.income;

}



function init_each_land_visual(game) {
    console.log('updating isuals')
    var game_container = document.getElementById('gameContainer');
    game_container.style.width=game.state.players.length*row_length*grid_width+'px';
    var map_div = document.getElementById('maps');
    map_div.appendChild(create_canvas(game.state.players.length));

    stage = new createjs.Stage("theCanvas");
    stage.enableMouseOver();

    var info_bar_div = document.getElementById('infoBar');
    for (var i = 0; i < game.state.players.length; i++) {
        var playerId = game.state.players[i].id;
        var player =  game.state.players[i];
        if (playerId == my_id) {
            my_index = i;
        }
        //var land = gamestate.lands[player];
        console.log("init: player:" + playerId + " - " + player.life + " - " + player.gold + 'g - ' + player.income);
        create_bg(i, my_index == i);
        draw_path(i, game.state.map['path']);
        var c = createInfoBarBox(game.state.players[i]);
        info_bar_div.appendChild(c);
    }
    //TODO extra indication of my map!
    //drawCreepSelector(gamestate.lands[gamestate.players[my_index]].creepTypes);

    stage.update();
}



function create_bg(index, active) {
    for (var i = 0; i < col_length * row_length; i++) {
        var s = new createjs.Shape();
        s.overColor = "#997777";
        s.outColor = "#443333";
        s.pathColor = "#cc9977";
        s.graphics.beginFill(s.outColor).drawRect(0, 0, grid_width, grid_height).endFill();
        s.x = ((grid_width ) * (i % row_length)) + (index * row_length * grid_width);
        s.y = (grid_height ) * (i / row_length | 0);

        if (active) {
            s.on('mouseover', function (event) {
                var target = event.target;
                target.graphics.clear().beginFill(target.overColor).drawRect(0, 0, grid_width, grid_height).endFill();
                stage.update();
            });
            s.on('mouseout', function (event) {
                var target = event.target;
                target.graphics.clear().beginFill(target.outColor).drawRect(0, 0, grid_width, grid_height).endFill();
                stage.update();
            });
            s.on('mousedown', function (e) {
                    var x = Math.floor((e.stageX -(my_index*grid_width*row_length))/ grid_width )  +.5;
                    var y = Math.floor(e.stageY / grid_width)  +.5;
                    clientSocket.emit('tower_buy',{x:x,y:y});
                }
            );
        }
        stage.addChild(s);
    }

}

function drawCreepSelector(creepTypes) {

    for (var i = 0; i < creepTypes.length; i++) {
        var s = new createjs.Shape();
        s.overColor = "#123456";
        s.outColor = "#123456";
        s.pathColor = "#cc9977";
        s.graphics.beginFill(s.overColor).drawRect(0, 0, grid_width, grid_height).endFill();
        s.x = grid_width * i + grid_width;
        s.y = (grid_height * col_length) + grid_height;
        console.log(creepTypes);
        s.creepType = creepTypes[i];

        s.on('mouseover', function (event) {
            var target = event.target;
            document.getElementById('creepInfo').innerHTML = target.creepType;

        });
        s.on('mouseout', function (event) {
            var target = event.target;
            document.getElementById('creepInfo').innerHTML = ''
        });

        s.on('mousedown', function (event) {
            var target = event.target;
            console.log('creating creep of type:'+target.creepType);
            buyCreep(target.creepType);
        });

        stage.addChild(s);

    }
    stage.update();

}

function draw_path(index, path) {

    for (var i = 0; i < path.length - 3; i += 2) {
        x0 = path[i] * grid_width + (index * row_length * grid_width);
        y0 = path[i + 1] * grid_height;
        x1 = path[i + 2] * grid_width + (index * row_length * grid_width);
        y1 = path[i + 3] * grid_height;
        var line = new createjs.Shape();
        line.graphics.moveTo(x0, y0).setStrokeStyle(1).beginStroke("#00ff00").lineTo(x1, y1);
        stage.addChild(line);
    }
    stage.update();
}


