/**
 * Created by edgera on 12/3/2014.
 */

EnactAction = {
    tower_buy: function (gamestate) {
        tower = new Tower(0, 0);
        Engine.buy_tower(tower, this.playerId, gamestate);
        return gamestate;
    }

}


var tower_buy = {
    type: 'tower_buy',
    tick: {},
    towerjson: {},
    playerId: {}
}

