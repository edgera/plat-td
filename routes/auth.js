var express = require('express');
var router = express.Router();

var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var configAuth = require('../config/auth');

var orm = require("orm");
var conString = configAuth.pgConnectionString;
var db = orm.connect(conString);

db.load("../app/models/User", function (err) {
    User = db.models.user;
});
db.sync();

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.get(id, function (err, user) {
        done(err, user);
    })
});

// code for login (use('local-login', new LocalStategy))
// code for signup (use('local-signup', new LocalStategy))
// code for facebook (use('facebook', new FacebookStrategy))
// code for twitter (use('twitter', new TwitterStrategy))

// =========================================================================
// GOOGLE ==================================================================
// =========================================================================
passport.use(new GoogleStrategy({
        clientID: configAuth.googleAuth.clientID,
        clientSecret: configAuth.googleAuth.clientSecret,
        callbackURL: configAuth.googleAuth.callbackURL
    },
    function (token, refreshToken, profile, done) {
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function () {
            User.find({ google_id: profile.id}, function (err, users) {
                console.log('logged in: ' + profile.id);

                if (err)
                    return done(err);

                if (users.length > 0) {
                    user = users[0];

                } else {
                    var newUser = new User();
                    newUser.google_id = profile.id;
                    newUser.google_token = token;
                    newUser.google_name = profile.displayName;
                    newUser.google_email = profile.emails[0].value; // pull the first email

                    newUser.save(function (err) {
                        console.log('error saving:' + err);
                    });

                    user = users[0];
                }
                done(err, user);

            });
        });
    })
);


// actual routing
router.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));
router.get('/google/return',
    passport.authenticate('google', {
        successRedirect: '/',
        failureRedirect: '/'
    }));

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});




module.exports = router;
